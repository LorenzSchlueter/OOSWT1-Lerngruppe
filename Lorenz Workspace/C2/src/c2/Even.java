package c2;

public class Even implements NotNull {
	
	public static Even create(NotNull e) {
		return new Even(e);
	}
	
	private final NotNull e;
	
	private Even(NotNull e) {
		this.e = e;
	}
	
	public boolean equals(Object argument) {
		if(this == argument) return true;
		if(!(argument instanceof Even)) return false;
		return this.e.equals(((Even)e).e);
	}

	@Override
	public long toCard() {
		return this.e.toCard() * 2;
	}
}
