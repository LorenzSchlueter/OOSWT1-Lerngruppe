package c2;

public class Odd implements NotNull {
	
	public static Odd create(C2 o) {
		return new Odd(o);
	}
	
	private final C2 o;
	
	private Odd(C2 o) {
		this.o = o;
	}

	public boolean equals(Object argument) {
		if(argument == this) return true;
		if(!(argument instanceof Odd)) return false;
		return this.o.equals(((Odd)argument).o);
	}

	@Override
	public long toCard() {
		return (this.o.toCard() * 2) + 1;
	}
}
