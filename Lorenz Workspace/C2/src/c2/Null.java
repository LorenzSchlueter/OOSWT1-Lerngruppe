package c2;

public class Null implements C2 {

	private static Null theNull = null;
	
	public static Null getOrCreate() {
		if(theNull == null) theNull = new Null();
		return theNull;
	}
	
	private Null() {}
	
	public boolean equals(Object argument) {
		return this == argument;
	}

	@Override
	public long toCard() {
		return 0;
	}
}
