package c2Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import c2.Even;
import c2.Null;
import c2.Odd;

class C2Test {

	@Test
	void test() {
		assertEquals(0,Null.getOrCreate().toCard());
		assertEquals(1,Odd.create(Null.getOrCreate()).toCard());
		assertEquals(2,Even.create(Odd.create(Null.getOrCreate())).toCard());
		assertEquals(3,Odd.create(Odd.create(Null.getOrCreate())).toCard());
		assertEquals(4,Even.create(Even.create(Odd.create(Null.getOrCreate()))).toCard());
	}

}
