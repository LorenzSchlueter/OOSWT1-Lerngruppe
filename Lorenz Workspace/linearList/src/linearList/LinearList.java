package linearList;

public interface LinearList {
	
	public boolean equals(Object argument);
	
	public long length();
	public LinearList addFirst(Object newFirst);
	public LinearList addLast(Object newLast);
	public LinearList removeLast() throws Exception;
	public boolean isEmpty();
	public LinearList concat(LinearList end);
}
