package linearList;

public class Filled implements LinearList{
	
	public static Filled create(Object first, LinearList rest) {
		if (first == null) throw new Error();
		if (rest == null)throw new Error();
		return new Filled(first, rest);
	}
	
	private Object first;
	private LinearList rest;
	
	private Filled(Object first, LinearList rest) {
		this.first = first;
		this.rest = rest;
	}
	
	@Override
	public boolean equals(Object argument) {
		if(super.equals(argument)) return true;
		if(!(argument instanceof Filled)) return false;
		Filled argumentAsFilled = (Filled) argument;
		return this.first.equals(argumentAsFilled.first) && this.rest.equals(argumentAsFilled.rest);
	}

	@Override
	public long length() {
		return this.rest.length() + 1;
	}

	@Override
	public LinearList addFirst(Object newFirst) {
		return Filled.create(newFirst, this);
	}

	@Override
	public LinearList addLast(Object newLast) {
		return Filled.create(this.first, this.rest.addLast(newLast));
	}

	@Override
	public LinearList removeLast() throws Exception {
		return this.rest.isEmpty() ? this.rest : Filled.create(this.first, this.rest.removeLast());
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public LinearList concat(LinearList end) {
		return Filled.create(this.first, this.rest.concat(end));
	}
	

}
