package linearList;

public class Empty implements LinearList{
	
	private static Empty theEmpty;

	public static Empty getOrCreate() {
		if(theEmpty == null) theEmpty = new Empty();
		return theEmpty;
	} 
	private Empty() {}
	
	public boolean equals(Object argument) {
		return argument instanceof Empty;
	}
	
	@Override
	public long length() {
		return 0;
	}
	@Override
	public LinearList addFirst(Object newFirst) {
		return Filled.create(newFirst, this);
	}
	@Override
	public LinearList addLast(Object newLast) {
		return this.addFirst(newLast);
	}
	@Override
	public LinearList removeLast() throws Exception {
		throw new Exception();
}
	@Override
	public boolean isEmpty() {
		return true;
	}
	@Override
	public LinearList concat(LinearList end) {
		return end;
	}
}
