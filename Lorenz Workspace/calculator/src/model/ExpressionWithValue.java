package model;

import observer.Observee;

public abstract class ExpressionWithValue extends Observee implements Expression{
	
	protected int value;
	protected String name;
	
	protected ExpressionWithValue() {
	
	}

	public ExpressionWithValue(int initialValue, final String name) {
		this.value = initialValue;
		this.name = name;
	}

	@Override
	public String getName(){
		return this.name;
	}

	@Override
	public String toString(){
		return this.getName() + "(" + this.getValue() + ")";
	}

	@Override
	public int getValue(){
		return this.value;
	}

}
