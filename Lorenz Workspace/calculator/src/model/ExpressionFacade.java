
package model;
import java.util.Collection;
import java.util.Vector;

import operations.Add;
import operations.Div;
import operations.Mul;
import operations.Sub;

public class ExpressionFacade {
	
	public static ExpressionFacade createExpressionFacade(){
		return new ExpressionFacade(new Vector<Expression>(), new Vector<Variable>());
	}

	private final Collection<Expression> expressions;
	private final Collection<Variable> variables;
	
	private ExpressionFacade(final Collection<Expression> expressions, final Collection<Variable> variables){
		this.expressions = expressions;
		this.variables = variables;
	}
	public void createVariable(final String name) {
		final Variable newVariable = Variable.createVariable(name);
		this.getExpressions().add(newVariable);
		this.getVariables().add(newVariable);
	}
	public Collection<Expression> getExpressions(){
		return this.expressions;
	}
	public Collection<Variable> getVariables(){
		return this.variables;
	}
	public void up(final Variable variable) {
		variable.up();
	}
	public void down(final Variable variable) {
		variable.down();
	}
	
	// TODO avoid cycles !!! 
	
	public void createAdd(final Expression firstArgument, final Expression secondArgument) {
		// TODO implement "createAdd" and store in expression list, compare "createVariable"!
		final Add addExpression = Add.create(firstArgument, secondArgument);
		this.getExpressions().add(addExpression);
		
	}
	public void createSubtract(final Expression firstArgument, final Expression secondArgument) {
		// TODO implement "createSubtract" and store in expression list, compare "createVariable"!
		final Sub subExpression = Sub.create(firstArgument, secondArgument);
		this.getExpressions().add(subExpression);
		
	}
	public void createMultiply(final Expression firstArgument, final Expression secondArgument) {
		// TODO implement "createMultiply" and store in expression list, compare "createVariable"!
		final Mul mulExpression = Mul.create(firstArgument, secondArgument);
		this.getExpressions().add(mulExpression);
		
	}
	public void createDivide(final Expression firstArgument, final Expression secondArgument) {
		// TODO implement "createDivide" and store in expression list, compare "createVariable"!
		final Div divExpression = Div.create(firstArgument, secondArgument);
		this.getExpressions().add(divExpression);
	}

}
