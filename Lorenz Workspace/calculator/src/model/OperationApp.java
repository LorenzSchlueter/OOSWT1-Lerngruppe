package model;

import observer.Observer;

public abstract class OperationApp extends ExpressionWithValue implements Observer{

	protected Expression argument1;
	protected Expression argument2;
	
	
	protected OperationApp(Expression arg1, Expression arg2) {
		super();
		this.argument1 = arg1;
		this.argument2 = arg2;
		this.op();
		this.createName();
		this.argument1.registerObserver(this);
		this.argument2.registerObserver(this);
	}
	
	@Override
	public void update() {
		this.op();
		this.notifyObservers();
	}
	
	public abstract void op();
	public abstract void createName();
}
