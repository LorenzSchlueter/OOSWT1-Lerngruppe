package model;

import observer.Observer;

public interface Expression{

	String getName();

	int getValue();

	void registerObserver(Observer o);
	
}
