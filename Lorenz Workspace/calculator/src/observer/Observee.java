package observer;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import model.ExpressionWithValue;

abstract public class Observee{
	
	private Collection<Observer> currentObservers;
	
	protected Observee (){
		this.currentObservers = new LinkedList<Observer>();
	}
	public void registerObserver(Observer observer) {
		this.currentObservers.add(observer);
	}
	public void deregisterObserver(Observer observer) {
		this.currentObservers.remove(observer);
	}
	public void notifyObservers() {
		Iterator<Observer> currentObs = this.currentObservers.iterator();
		while (currentObs.hasNext())   {
			Observer current = currentObs.next();
			current.update();
		}
	}
}
