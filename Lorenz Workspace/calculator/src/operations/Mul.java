package operations;

import model.Expression;
import model.OperationApp;

public class Mul extends OperationApp {
	
	public static Mul create(Expression arg1, Expression arg2) {
		return new Mul(arg1, arg2);
	}

	public Mul(Expression arg1, Expression arg2) {
		super(arg1, arg2);
	}

	@Override
	public void op() {
		this.value = this.argument1.getValue() * this.argument2.getValue();
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public void createName(){
		this.name =  "Mul[" + this.argument1.getName() + ", " + this.argument2.getName() + "](" + this.getValue() + ")";
	}

	@Override
	public int getValue() {
		return this.value;
	}
}
