package operations;

import model.Expression;
import model.OperationApp;

public class Add extends OperationApp {
		
	public static Add create(Expression arg1, Expression arg2) {
		return new Add(arg1, arg2);
	}

	public Add(Expression arg1, Expression arg2) {
		super(arg1, arg2);
	}

	@Override
	public void op() {
		this.value = this.argument1.getValue() + this.argument2.getValue();
	}
	
	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public void createName(){
		this.name =  "Add[" + this.argument1.getName() + ", " + this.argument2.getName() + "](" + this.getValue() + ")";
	}

	@Override
	public int getValue() {
		return this.value;
	}

}
