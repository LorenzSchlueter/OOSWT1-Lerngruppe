package logic;

public interface Bool3 {

	public Bool3 neg();
	public Bool3 and(Bool3 argument);
	public Bool3 andPerhaps();
}
