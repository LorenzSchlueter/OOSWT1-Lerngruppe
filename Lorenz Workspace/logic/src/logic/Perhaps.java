package logic;

public class Perhaps implements Bool3{
	
	private static Perhaps thePerhaps;
	
	public static Perhaps getOrCreate() {
		if(thePerhaps == null) thePerhaps = new Perhaps();
		return thePerhaps;
	}
	
	private Perhaps(){
	}
	
	public boolean equals(Object argument) {
		return argument instanceof Perhaps;
	}
	
	@Override
	public Perhaps neg() {
		return this;
	}

	@Override
	public Bool3 and(Bool3 argument) {
		return argument.andPerhaps();
	}

	@Override
	public Bool3 andPerhaps() {
		return this;
	}
}
