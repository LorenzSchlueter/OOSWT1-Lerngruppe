package logic;

public class False implements Bool3{
	
private static False theFalse;
	
	public static False getOrCreate() {
		if(theFalse == null)theFalse = new False();
		return theFalse;
	}
	
	private False() {
	}
	
	public boolean equals(Object argument) {
		return (argument instanceof False);
	}
	
	@Override
	public True neg() {
		return True.getOrCreate();
	}

	@Override
	public Bool3 and(Bool3 argument) {
		return this;
	}

	@Override
	public Bool3 andPerhaps() {
		return this;
	}
}
