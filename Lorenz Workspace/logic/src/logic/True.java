package logic;

public class True implements Bool3{
	
	private static True theTrue;
	
	public static True getOrCreate() {
		if(theTrue == null)theTrue = new True();
		return theTrue;
	}
	
	private True() {
	}
	
	public boolean equals(Object argument) {
		return (argument instanceof True);
	}
	
	@Override
	public False neg() {
		return False.getOrCreate();
	}

	@Override
	public Bool3 and(Bool3 argument) {
		return argument;
	}

	@Override
	public Bool3 andPerhaps() {
		return Perhaps.getOrCreate();
	}
}
