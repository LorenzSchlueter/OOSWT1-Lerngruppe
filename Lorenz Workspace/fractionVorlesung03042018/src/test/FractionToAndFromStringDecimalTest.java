package test;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

import fraction.Fraction;

class FractionToAndFromStringDecimalTest {	
	
	private void assertDecimalInt(String enumerator, String denominator) {
		final String s1 = Fraction.create(BigInteger.valueOf(Long.valueOf(enumerator)),BigInteger.valueOf(Long.valueOf(denominator))).toString();
		
		assertEquals(enumerator, s1);
		
		try {
			assertEquals(s1, Fraction.fromString(enumerator).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void assertDecimalDouble(String enumerator, String denominator, String stringRepresentation) {
		final String s1 = Fraction.create(BigInteger.valueOf(Long.valueOf(enumerator)),BigInteger.valueOf(Long.valueOf(denominator))).toString();
				
		try {
			assertEquals(s1, Fraction.fromString(stringRepresentation).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void naturalNumberPositiveTest1() {
		assertDecimalInt("5", "1");
	}
	
	@Test
	void integerTest1() {
		assertDecimalInt("-5","1");
	}
	
	@Test
	void decimalTest1() {
		assertDecimalDouble("1", "8", "0.125");
	}
	
	@Test
	void decimalTest2() {
		assertDecimalDouble("-1", "2", "-0.5");
	}
}
