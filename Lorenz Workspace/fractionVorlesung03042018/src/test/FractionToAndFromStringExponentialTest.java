package test;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

import fraction.Fraction;

class FractionToAndFromStringExponentialTest {	
	
	@Test
	void exponentialTest1() {
		final String s1 = Fraction.create(BigInteger.valueOf(5), BigInteger.valueOf(10)).toString();
		
		assertEquals("1/2", s1);
		
		try {
			assertEquals(s1, Fraction.fromString("5E-1").toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void exponentialTest2() {
		final String s1 = Fraction.create(BigInteger.valueOf(50000000000000L), BigInteger.valueOf(1)).toString();
		
		assertEquals("50000000000000", s1);
		
		try {
			assertEquals(s1, Fraction.fromString("5E+13").toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void exponentialTest3() {
		final String s1 = Fraction.create(BigInteger.valueOf(5123L), BigInteger.valueOf(100)).toString();
		
		assertEquals("5123/100", s1);
		
		try {
			assertEquals(s1, Fraction.fromString("5123E-2").toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void exponentialTest4() {
		final String s1 = Fraction.create(BigInteger.valueOf(5123L), BigInteger.valueOf(100000)).toString();
		
		assertEquals("5123/100000", s1);
		
		try {
			assertEquals(s1, Fraction.fromString("5.123E-2").toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
