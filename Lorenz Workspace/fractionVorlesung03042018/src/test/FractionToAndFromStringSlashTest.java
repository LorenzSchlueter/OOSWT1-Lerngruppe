package test;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

import fraction.Fraction;

class FractionToAndFromStringSlashTest {

	@Test
	void test() {
		final String s1 = Fraction.create(BigInteger.valueOf(17),BigInteger.valueOf(19)).toString();
		assertEquals("17/19",s1);
		
		try {
			assertEquals(s1, Fraction.fromString(s1).toString());
			assertEquals("17/19",Fraction.fromString("17/19").toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	  void testBruchFromStringMitSlash() {
	    try {
	    	Fraction f1 = Fraction.fromString("3/5");
		    Fraction f2 = Fraction.create(BigInteger.valueOf(3), BigInteger.valueOf(5));
		    assertEquals(f1 , f2);
		    assertEquals(f1.toString() , f2.toString());
		    assertEquals("3/5" , f2.toString());
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	  }
	
	  @Test
	  void testBruchFromStringMitSlashEintel() {
	    try {
	    	final Fraction f1 = Fraction.fromString("2/1");
		    final Fraction f2 = Fraction.create(BigInteger.valueOf(2), BigInteger.valueOf(1));
		    assertEquals(f1 , f2);
		    assertEquals(f1.toString() , f2.toString());
		    assertEquals("2" , f2.toString());
	    }catch (Exception e) {
	    	e.printStackTrace();
		}
	  }

	  
}
