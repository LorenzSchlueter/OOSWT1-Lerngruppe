package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

import fraction.Fraction;


class FractionTest {

	private final Fraction zero = Fraction.create(BigInteger.valueOf(0), BigInteger.valueOf(1));
	private final Fraction one = Fraction.create(BigInteger.valueOf(1), BigInteger.valueOf(1));
	private final Fraction minusOne = Fraction.create(BigInteger.valueOf(1), BigInteger.valueOf(-1));
	private final Fraction two = Fraction.create(BigInteger.valueOf(2), BigInteger.valueOf(1));

	private final Fraction half = Fraction .create(BigInteger.valueOf(1), BigInteger.valueOf(2));
	private final Fraction quarter = Fraction .create(BigInteger.valueOf(1), BigInteger.valueOf(4));
	private final Fraction twoQuarters = Fraction.create(BigInteger.valueOf(2), BigInteger.valueOf(4));
	private final Fraction threeQuarters = Fraction.create(BigInteger.valueOf(3), BigInteger.valueOf(4));
	private final Fraction threeHalves = Fraction.create(BigInteger.valueOf(3), BigInteger.valueOf(2));
	private final Fraction oneThird = Fraction.create(BigInteger.valueOf(3), BigInteger.valueOf(9));

	@Test
	void testEqualsFramework() {
		assertEquals(half,twoQuarters);	
	}
	@Test
	void testMul() {
		assertEquals(zero, half.multiply(zero));
		assertEquals(zero, one.multiply(zero));
		assertEquals(two, two.multiply(one));
		assertEquals(quarter, half.multiply(half));
		assertEquals(one, twoQuarters.multiply(two));
		assertEquals(minusOne, minusOne.multiply(one));
		assertEquals(minusOne, one.multiply(minusOne));
		assertEquals(one, minusOne.multiply(minusOne));
	}
	@Test
	void testDiv() {
		assertEquals(two, two.divide(one));
		assertEquals(one, half.divide(half));
		assertEquals(quarter, twoQuarters.divide(two));
		assertEquals(threeHalves, threeQuarters.divide(half));
		assertEquals(minusOne, minusOne.divide(one));
		assertEquals(minusOne, one.divide(minusOne));
		assertEquals(one, minusOne.divide(minusOne));
	}
	@Test
	void testDivExceptional() {
		try {
			quarter.divide(zero);		
			fail();
		} catch (ArithmeticException e) {
			return;
		}
	}
	@Test
	void testAdd() {
		assertEquals(two, one.add(one));
		assertEquals(one, half.add(half));
		assertEquals(threeHalves, threeQuarters.add(threeQuarters));
		assertEquals(two, oneThird.add(threeQuarters).add(oneThird).add(quarter).add(oneThird));
	}
	@Test
	void testSubtract() {
		assertEquals(zero, one.subtract(one));
		assertEquals(zero, half.subtract(half));
		assertEquals(half, threeHalves.subtract(threeQuarters).subtract(quarter));
		assertEquals(zero, oneThird.subtract(threeQuarters).add(oneThird).subtract(quarter).add(oneThird));
	}
	
	@Test
	void toStringFromString() {
		String halfS = half.toString();
		try {
			assertEquals(half, Fraction.fromString(halfS));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
}
