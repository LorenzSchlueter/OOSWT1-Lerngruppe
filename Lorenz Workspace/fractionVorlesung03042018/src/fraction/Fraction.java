package fraction;

import java.math.BigInteger;

/**Objects of this class represent values of the rational numbers. 
 * All operations do not change the value of the receiver. */
public class Fraction {
	
	private static final String DIVISION_BY_ZERO = "Division by zero!";
	private static final BigInteger MINUS_ONE = BigInteger.ZERO.subtract(BigInteger.ONE);
	
	/**
	 * 
	 * @param fractionString
	 * @return
	 * @throws Exception
	 */
	public static Fraction fromString(String fractionString) throws Exception {
		if(fractionString.contains("/")) return handleSlashed(fractionString);
		if(fractionString.contains("E")) return handleExponential(fractionString);
		return handleDecimal(fractionString);
		
	}

	/**
	 * Converts an input in form of the "slashed representation" of a fraction into a Fraction.
	 * Valid inputs have the form: "a/b" , "a / b", "a /b", etc. Whitespaces at the beginning and end of the enumerator and denumerator are being ignored..
	 * @param fractionString String Representation of the Fraction.
	 * @return the Fractions representation of the string.
	 * @throws Exception if the input can not be converted.
	 */
	private static Fraction handleSlashed(String fractionString) throws Exception {
		String[] parts = fractionString.split("/");
		
		/*
		 * TODO
		 */
		if(!(parts.length == 1 || parts.length ==2)) {
			throw new ArithmeticException("Falsches Format!");
		}
		
		if(parts.length == 1) return Fraction.create(BigInteger.ONE, new BigInteger(parts[0]));
		return Fraction.create(new BigInteger(parts[0].trim()), new BigInteger(parts[1].trim()));
	}
	/**
	 * Converts an input in form of the "scientific notation" of a fraction into a Fraction.
	 * Valid inputs have the form (r = any rational number, n = any natural number): "rE-n" , "rE+n", "r E-n", etc.. Whitespaces at the beginning and end of numerical parts are being ignored..
	 * @param input the scientific notation of the number
	 * @return  the Fraction representation of the input in scientific notation
	 * @throws Exception if the input is malformed.
	 */
	private static Fraction handleExponential(String input) throws Exception{
		String[] parts = input.split("E");
		
		if(parts.length != 2) {
			throw new Exception("Falsches Format");
		}
		
		/*We create a Fraction of the unscaled Value of the first decimal part of the notation. 
		* This means for example "5.12345E10" has an unscaled Value of "512345/1".
		*/
		Fraction unscaledValue = handleDecimal(parts[0].trim());
		
		//Now we determin by what factor the unscaled Value should be scaled. This is the left/right shifting of the comma.
		Integer scaleValue = Integer.valueOf(parts[1].trim());
		Fraction scale = null;
			BigInteger theFactor = BigInteger.TEN.pow((int) Math.sqrt(Math.pow(scaleValue,2)));
			
			//Negative and positive exponents need to be handled different. 
			//Negative exponents (e) result in the scale  "1/ abs(e)", positive ones in "abs(e) / 1"
			if (scaleValue < 0) {
				scale = Fraction.create(BigInteger.ONE, theFactor);
			} else {
				scale = Fraction.create(theFactor, BigInteger.ONE);

			}
			
		//Now we need to scale the unscaledValue.	
		return unscaledValue.multiply(scale);
	}
	
	/**
	 * Converts any rational number in the "x.xxx" or "x" notation into an Fraction.
	 */
	private static Fraction handleDecimal(String fractionString) throws Exception {
		
		if(fractionString.contains(".")) {
			Integer posSeperator = fractionString.indexOf(".");
			String vorkommaAnteil = fractionString.substring(0, posSeperator);
			String nachkommaAnteil = removeEndingZeros(fractionString.substring(posSeperator + 1, fractionString.length()));
			Fraction unscaledaValue = Fraction.create(new BigInteger(vorkommaAnteil + nachkommaAnteil), BigInteger.ONE);
			Fraction scale = Fraction.create(BigInteger.ONE, BigInteger.TEN.pow(nachkommaAnteil.length()));
			
			return unscaledaValue.multiply(scale);
			
		}
		
		return handleNormal(fractionString);
	}
	
	/**
	 * Handles the conversion from normal integer numbers to fractions.
	 * @param fractionString the string representation of the integer.
	 * @return the Fraction representation of the integer
	 */
	private static Fraction handleNormal(String fractionString) {
		String pattern = "[^\\d]";
		if(fractionString.matches(pattern)) {
			throw new ArithmeticException("Falsches Format");
		}
		return Fraction.create(new BigInteger(fractionString), BigInteger.ONE);
	}
	
	/**
	 * Removes the ending zeros in a String. If an "." is at the strings lasts position, it gets removed too. 
	 * @param input The string to be modified.
	 * @return
	 */
	private static String removeEndingZeros(String input) {
		if(input.endsWith("0") || input.endsWith(".")) {
			return removeEndingZeros(input.substring(0,input.length() - 1));
		}
		return input;
	}
	
	
	/**Returns a normalized fraction representing the value enumerator/denominator.
	 * Throws an arithmetic exception, if the denominator is zero. 
	 * @param enumerator unnormalized value for the result's enumerator.
	 * @param denominator unnormalized value for the result's denominator.
	 */
	public static Fraction create(final BigInteger enumerator, final BigInteger denominator) {
		if (denominator.equals(BigInteger.ZERO)) throw new ArithmeticException(DIVISION_BY_ZERO);
		final BigInteger gcd = enumerator.gcd(denominator);
		final boolean denominatorIsNegative = denominator.compareTo(BigInteger.ZERO) < 0;
		final BigInteger signHandledEnumerator = denominatorIsNegative ? enumerator.multiply(MINUS_ONE) : enumerator;
		final BigInteger signHandledDenominator = denominatorIsNegative ? denominator.multiply(MINUS_ONE) : denominator;
		return new Fraction(signHandledEnumerator.divide(gcd),signHandledDenominator.divide(gcd));
	}

	final private BigInteger enumerator;
	final private BigInteger denominator;
	
	private Fraction(final BigInteger enumerator, final BigInteger denominator) {
		this.enumerator = enumerator;
		this.denominator = denominator;	
	}
	/**
	 * s
	 */
	public String toString() {
		
		if(this.denominator.equals(BigInteger.ONE)) {
			return this.enumerator.toString();
		}
		
		return this.enumerator.toString() + "/" + this.denominator.toString();

	}
	/** Returns true if and only if the argument represents the same value as the receiver. */
	private boolean equalsAsFraction(final Fraction argument) {
		return this.enumerator.multiply(argument.denominator).equals(this.denominator.multiply(argument.enumerator));
	}
	/***/
	public boolean equals(final Object argument) {
		return (argument instanceof Fraction) ? this.equalsAsFraction((Fraction)argument) : false;
	}
	/** Returns a new normalised fraction object representing the product of the receiver and factor. */
	public Fraction multiply(final Fraction factor) {
		final BigInteger resultEnumerator = this.enumerator.multiply(factor.enumerator);
		final BigInteger resultDenominator = this.denominator.multiply(factor.denominator);
		return Fraction.create(resultEnumerator, resultDenominator);
	}
	/** Returns a new normalised fraction object representing the quotient of the receiver (dividend) and divisor. */
	public Fraction divide(final Fraction divisor) {
		final BigInteger resultEnumerator = this.enumerator.multiply(divisor.denominator);
		final BigInteger resultDenominator = this.denominator.multiply(divisor.enumerator);
		return Fraction.create(resultEnumerator, resultDenominator);
	}
	/** Returns a new normalised fraction object representing the sum of the receiver and summand. */
	public Fraction add(final Fraction summand) {
		final BigInteger resultEnumerator = this.enumerator.multiply(summand.denominator)
											 .add(summand.enumerator.multiply(this.denominator));
		final BigInteger resultDenominator = this.denominator.multiply(summand.denominator);
		return Fraction.create(resultEnumerator, resultDenominator);
	}
	/** Returns a new normalised fraction object representing the difference of the receiver (minuend) and subtrahend. */
	public Fraction subtract(final Fraction subtrahend) {
		final BigInteger resultEnumerator = this.enumerator.multiply(subtrahend.denominator)
											 .subtract(subtrahend.enumerator.multiply(this.denominator));
		final BigInteger resultDenominator = this.denominator.multiply(subtrahend.denominator);
		return Fraction.create(resultEnumerator, resultDenominator);
	}

}












