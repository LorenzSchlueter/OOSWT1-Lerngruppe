package model;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;


public class Product extends ComponentCommon {

	private static final String CycleMessage = "Zyklen sind in der Aufbaustruktur nicht erlaubt!";

	public static Product create(final String name, final int price) {
		return new Product(name,new HashMap<Component, QuantifiedComponent>(), price);
	}
	private final Map<Component,QuantifiedComponent> components;
	
	protected Product(final String name, final Map<Component,QuantifiedComponent> components, final int price) {
		super(name, price);
		this.components = components;
	}

	@Override
	public void addPart(final Component part, final int amount) throws Exception{
		if (part.contains(this))throw new Exception(CycleMessage);
		if (components.containsKey(part)){
			final QuantifiedComponent oldQuantification = this.components.get(part); 
			oldQuantification.addQuantity(amount);
		}else{
			this.components.put(part, QuantifiedComponent.createQuantifiedComponent(amount, part));
		}
	}

	@Override
	public boolean contains(final Component component) {
		if (this.equals(component)) return true;
		final Iterator<QuantifiedComponent> i = this.components.values().iterator();
		while (i.hasNext()){
			final QuantifiedComponent current = i.next();
			if (current.contains(component))return true;
		}
		return false;
	}

	@Override
	public Vector<QuantifiedComponent> getDirectParts() {
		return new Vector<QuantifiedComponent>(this.components.values());
	}

	@Override
	public int getNumberOfMaterials() {
		int result = 0;
		final Iterator<QuantifiedComponent> i = this.components.values().iterator();
		while (i.hasNext()){
			final QuantifiedComponent current = i.next();
			result = result + current.getNumberOfMaterials();
		}
		return result;
	}

	@Override
	public Vector<QuantifiedComponent> getMaterialList() {
		MaterialList materialList = MaterialList.create();
		Vector<QuantifiedComponent> components = new Vector<QuantifiedComponent>(this.components.values());
		Iterator<QuantifiedComponent> componentIterator = components.iterator();
		while(componentIterator.hasNext()) {
			QuantifiedComponent current = componentIterator.next();
			Vector<QuantifiedComponent> myMaterials = current.getComponent().getMaterialList();
			Iterator<QuantifiedComponent> myMaterialIterator = myMaterials.iterator();
			int currentQuantity = current.getQuantity();
			while(myMaterialIterator.hasNext()) {
				QuantifiedComponent currentMaterial = myMaterialIterator.next();
				materialList.add(currentMaterial.getComponent(), currentMaterial.getQuantity() * currentQuantity);
			}
			
		}
		return materialList.getMaterialList();
	}

	@Override
	public int getOverallPrice() {
		int result = super.price;
		Iterator<QuantifiedComponent> componentIterator = this.components.values().iterator();
		while(componentIterator.hasNext()) {
			QuantifiedComponent current = componentIterator.next();
			result = result + (current.getComponent().getOverallPrice() * current.getQuantity());
		}
		return result;
	}

	@Override
	public void changePrice(int newPrice) {
		this.price = newPrice;
	}


}
