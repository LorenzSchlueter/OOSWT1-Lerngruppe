package model;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


/**
 *  Represents a hierarchical partslist as a mapping from unique part names to components.
 */
public class PartsList {
	
	private static final String DoubleDefinitionMessage = "Name bereits vorhanden!";
	private static final String UnknownComponentMessage = "Unbekannte Komponente: ";

	/**
	 * @return An empty partslist.
	 */
	public static PartsList create (){
		return new PartsList(new HashMap<String, Component>());
	}
	final private Map<String, Component> componentsMap;
	
	private PartsList(final Map<String, Component> componentsMap){
		this.componentsMap = componentsMap;
	}

	@Override
	public boolean equals(Object argument) {
		return super.equals(argument);
	}
	/**
	 * Creates a new material with the given name and price as a component of the receiver.
	 * @throws Exception If the provided name is already used for another component of the receiver.
	 */
	public void createMaterial(final String name, final int price) throws Exception {
		if (this.componentsMap.containsKey(name))
			throw new Exception(DoubleDefinitionMessage);
		final Material newMaterial = Material.create(name, price);
		this.componentsMap.put(name, newMaterial);
	}
	/**
	 * Creates a new product with the given name and price as a component of the receiver.
	 * @throws Exception If the provided name is already used for another component of the receiver.
	 */
	public void createProduct(final String name, final int price) throws Exception {
		if (this.componentsMap.containsKey(name)) throw new Exception(DoubleDefinitionMessage);
		final Product newProduct = Product.create(name, price);
		this.componentsMap.put(name, newProduct);
	}
	/**
	 * Adds amount pieces of the component part as subparts of the component whole. 
	 * @throws Exception <ol> <li> If whole or part are not contained in the partslist of the receiver.</li>
	 *                        <li> If adding part as subpart of whole violates the hierarchy contraint of the partslist represented by the receiver.</li>
	 *                   </ol>
	 */
	public void addPart(final Component whole, final Component part, final int amount) throws Exception {
		if (!this.componentsMap.containsValue(whole)) throw new Exception(UnknownComponentMessage + whole.toString());
		if (!this.componentsMap.containsValue(part)) throw new Exception(UnknownComponentMessage + part.toString());
		whole.addPart(part,amount);
	}
	/**
	 * Returns the number of materials that are directly or indirectly parts of the given component.
	 * @throws Exception If component is not contained in the partslist of the receiver.
	 */
	public int getMaterialCount(final Component component) throws Exception {
		if (!this.componentsMap.containsValue(component)) throw new Exception(UnknownComponentMessage + component.toString());
		return component.getNumberOfMaterials();
	}
	public Vector<Component> getComponents() {
		return new Vector<Component>(this.componentsMap.values());
	}
	public Component getComponent(String name) {
		return this.componentsMap.get(name);
	}
	/**
	 * Returns the list of quantified parts that are the direct subparts of component.
	 * @throws Exception If component is not contained in the partslist of the receiver.
	 */
	public Vector<QuantifiedComponent> getParts(final Component component) throws Exception {
		if (!this.componentsMap.containsValue(component)) throw new Exception(UnknownComponentMessage + component.toString());
		return component.getDirectParts();
	}
	/**
	 * TODO Returns the list of all materials needed to construct the Compontent <component>
	 */
	public Vector<QuantifiedComponent> getMaterialList(final Component component) {
		return component.getMaterialList();
	}
	/**
	 * TODO Write comment for getOverallPrice!
	 */
	public String getOverallPrice(final Component component) {
		return "" + component.getOverallPrice();
	}

	/**
	 * TODO Write comment for changePrice!
	 */
	public void changePrice(final Component component, final int newPrice) {
		component.changePrice(newPrice);
	}
}
