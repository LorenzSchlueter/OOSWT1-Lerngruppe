package model;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class MaterialList {
	
	private Map<Component, QuantifiedComponent> theMaterialList;
	
	public static MaterialList create() {
		return new MaterialList();
	}
	
	private MaterialList() {
		this.theMaterialList = new HashMap<Component, QuantifiedComponent>();
	}

	public Vector<QuantifiedComponent> getMaterialList() {
		return new Vector<QuantifiedComponent>(this.theMaterialList.values());
		}

	public void add(Component current, int quantity) {
		if(this.theMaterialList.containsKey(current)) {
			this.theMaterialList.get(current).addQuantity(quantity);
		}else {
			this.theMaterialList.put(current, QuantifiedComponent.createQuantifiedComponent(quantity, current));
		}
	}

}
