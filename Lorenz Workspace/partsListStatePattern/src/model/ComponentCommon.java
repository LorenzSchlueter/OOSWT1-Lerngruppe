package model;


public abstract class ComponentCommon implements Component {

	private final String name;
	protected int price;

	protected ComponentCommon(final String name, final int price) {
		this.name = name;
		this.price = price;
	}
	@Override
	public String toString(){
		return name;
	}
	@Override
	public boolean equals(Object argument) {
		return super.equals(argument);
	}
}
