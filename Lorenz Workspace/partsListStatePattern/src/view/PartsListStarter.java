package view;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.DimensionUIResource;

import model.PartsList;
import tests.PresetPartsLists;


public class PartsListStarter {

	private static final int StandardResolution = 80;
	private static final int StandardFontSize = 12;
	static int fontSize;
	
	private static float scaleFactor;
	
	public static void main(final String[] args) {
		try {
		  UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		}
		catch(final Exception e) {
		  e.printStackTrace();
		}
		Toolkit toolKit = Toolkit.getDefaultToolkit();
		scaleFactor = (float) toolKit.getScreenResolution() / (float) StandardResolution;
		fontSize = (int)(StandardFontSize * scaleFactor);

		changeSystemFontSizes();
		
		View.setResolution(scaleFactor);
		
//		final View view = View.create(PartsList.create()); FIXME Uncomment for default PartsList
		final View view = View.create(PresetPartsLists.createBettPartsList());

		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		final Dimension viewSize = view.getSize();
		final double leftMargin = (screenSize.getWidth() - viewSize.getWidth())/2;
		final double topMargin = (screenSize.getHeight() - viewSize.getHeight())/2;
		view.setLocation((int)leftMargin,(int)topMargin);
		view.setVisible(true);
	}

	private static void changeSystemFontSizes() {
		java.util.Enumeration<Object> keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get (key);
			if (value != null && value instanceof FontUIResource){
				FontUIResource font = (FontUIResource) value;
				FontUIResource derivedFont = new FontUIResource(font.deriveFont((float)fontSize));
				UIManager.put (key, derivedFont);
			}
		}
		UIManager.put("ScrollBar.width", new Float(((Integer)UIManager.get("ScrollBar.width")) * (1 + (scaleFactor - 1) * 0.66)).intValue());
		UIManager.put("SplitPane.dividerSize", new Float(((Integer)UIManager.get("SplitPane.dividerSize")) * (1 + (scaleFactor - 1) * 0.66)).intValue());
		Dimension progressBarSize = ((DimensionUIResource) UIManager.get("ProgressBar.horizontalSize")).getSize();
		UIManager.put("ProgressBar.horizontalSize", new javax.swing.plaf.DimensionUIResource((int) progressBarSize.getWidth(), (int)(progressBarSize.getHeight() * scaleFactor)));

	}


}
