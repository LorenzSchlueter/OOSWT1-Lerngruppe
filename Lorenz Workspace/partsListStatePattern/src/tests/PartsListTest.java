package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Component;
import model.Material;
import model.PartsList;
import model.QuantifiedComponent;

class PartsListTest {
	
	private PartsList defaultPartsList = null;
	
	@BeforeEach
	void setup() {
		defaultPartsList = PresetPartsLists.createBettPartsList();
	}
	
	@Test
	void getMaterialListTest() {
		//DER TEST IST NICHT GUT! TODO Testfall �berarbeiten!!!!
		Component bett = defaultPartsList.getComponent(PresetPartsLists.BETT);
		Vector<QuantifiedComponent> resultList = defaultPartsList.getMaterialList(bett);
		QuantifiedComponent bettf��e = QuantifiedComponent.createQuantifiedComponent(4, Material.create(PresetPartsLists.BETTFU�,1));
		QuantifiedComponent schraubenUndM�tter = QuantifiedComponent.createQuantifiedComponent(8, Material.create(PresetPartsLists.SCHRAUBE_MUTTER,1));
		QuantifiedComponent matraze = QuantifiedComponent.createQuantifiedComponent(1, Material.create(PresetPartsLists.MATRAZE,1));
		QuantifiedComponent feder = QuantifiedComponent.createQuantifiedComponent(128, Material.create(PresetPartsLists.FEDER,1));
		QuantifiedComponent holzkleber = QuantifiedComponent.createQuantifiedComponent(39, Material.create(PresetPartsLists.HOLZKLEBER,1));
		QuantifiedComponent band = QuantifiedComponent.createQuantifiedComponent(32, Material.create(PresetPartsLists.BAND,1));
		QuantifiedComponent holzlatten = QuantifiedComponent.createQuantifiedComponent(68, Material.create(PresetPartsLists.HOLZLATTE,1));
		QuantifiedComponent h�bscheStudentin = QuantifiedComponent.createQuantifiedComponent(2, Material.create(PresetPartsLists.H�BSCHE_STUDENTIN,1));
		Vector<QuantifiedComponent> expectedList = new Vector<QuantifiedComponent>();
		expectedList.add(bettf��e);
		expectedList.add(schraubenUndM�tter);
		expectedList.add(matraze);
		expectedList.add(feder);
		expectedList.add(holzkleber);
		expectedList.add(band);
		expectedList.add(holzlatten);
		expectedList.add(h�bscheStudentin);
		
		if(!(resultList.size() == expectedList.size()))fail("Die Material-Listen enthalten unterschiedlich viele Komponenten! \n Result: " + resultList.size() + "\n Expected: " + expectedList.size());

		
		Collections.sort(resultList, new Comparator<QuantifiedComponent>() {

			@Override
			public int compare(QuantifiedComponent o1, QuantifiedComponent o2) {
				return o1.getComponent().toString().compareTo(o2.getComponent().toString());
			}
		});
		
		Collections.sort(expectedList, new Comparator<QuantifiedComponent>() {

			@Override
			public int compare(QuantifiedComponent o1, QuantifiedComponent o2) {
				return o1.getComponent().toString().compareTo(o2.getComponent().toString());
			}
		});
		
	assertEquals(expectedList.toString(), resultList.toString());
	}

	@Test
	void getOverallPriceTest() {
		Component bett = defaultPartsList.getComponent(PresetPartsLists.BETT);
		defaultPartsList.getOverallPrice(bett);
		int expectedPrice = 4 + 256 + 15 + 4 + 8 + 7 + 1 + 75 + 75 + 5 + 5;
		assertEquals(expectedPrice,bett.getOverallPrice());
	}
}
