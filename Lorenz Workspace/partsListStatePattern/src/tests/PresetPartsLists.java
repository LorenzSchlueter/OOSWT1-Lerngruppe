package tests;

import model.Component;
import model.PartsList;

public class PresetPartsLists {

	public static PartsList createBettPartsList() {
		PartsList defaultPartsList = PartsList.create();
		try {
			//Creating all needed Materials
			defaultPartsList.createMaterial(PresetPartsLists.BETTFU�, 1);
			defaultPartsList.createMaterial(PresetPartsLists.HOLZLATTE, 1);
			defaultPartsList.createMaterial(PresetPartsLists.SCHRAUBE_MUTTER, 1);
			defaultPartsList.createMaterial(PresetPartsLists.MATRAZE, 1);
			defaultPartsList.createMaterial(PresetPartsLists.FEDER, 1);
			defaultPartsList.createMaterial(PresetPartsLists.HOLZKLEBER, 1);
			defaultPartsList.createMaterial(PresetPartsLists.BAND, 1);
			defaultPartsList.createMaterial(PresetPartsLists.H�BSCHE_STUDENTIN, 75);
			
			Component bettfu� = defaultPartsList.getComponent(PresetPartsLists.BETTFU�);
			Component holzlatte = defaultPartsList.getComponent(PresetPartsLists.HOLZLATTE);
			Component schraubeUndMutter = defaultPartsList.getComponent(PresetPartsLists.SCHRAUBE_MUTTER);
			Component matraze = defaultPartsList.getComponent(PresetPartsLists.MATRAZE);
			Component feder = defaultPartsList.getComponent(PresetPartsLists.FEDER);
			Component holzkleber = defaultPartsList.getComponent(PresetPartsLists.HOLZKLEBER);
			Component band = defaultPartsList.getComponent(PresetPartsLists.BAND);
			Component huebscheStudentin = defaultPartsList.getComponent(PresetPartsLists.H�BSCHE_STUDENTIN);
	
			//Creating all Products
			defaultPartsList.createProduct(PresetPartsLists.BETTFU�PAKET, 0);
			defaultPartsList.createProduct(PresetPartsLists.LATTENROST, 15);
			defaultPartsList.createProduct(PresetPartsLists.BETTGESTELL, 5);
			defaultPartsList.createProduct(PresetPartsLists.BETT, 5);
			
			Component bettfu�Paket = defaultPartsList.getComponent(PresetPartsLists.BETTFU�PAKET);
			Component lattenrost = defaultPartsList.getComponent(PresetPartsLists.LATTENROST);
			Component bettgestell = defaultPartsList.getComponent(PresetPartsLists.BETTGESTELL);
			Component bett = defaultPartsList.getComponent(PresetPartsLists.BETT);
			
			defaultPartsList.addPart(bettfu�Paket, bettfu�, 4);
			defaultPartsList.addPart(lattenrost, holzlatte, 64);
			defaultPartsList.addPart(lattenrost, feder, 128);
			defaultPartsList.addPart(lattenrost, holzkleber, 32);
			defaultPartsList.addPart(lattenrost, band, 32);
			defaultPartsList.addPart(bettgestell, holzlatte, 4);
			defaultPartsList.addPart(bettgestell, schraubeUndMutter, 8);
			defaultPartsList.addPart(bettgestell, holzkleber, 7);
			defaultPartsList.addPart(bettgestell, bettfu�Paket, 1);
			defaultPartsList.addPart(bettgestell, lattenrost, 1);
			defaultPartsList.addPart(bett, bettgestell, 1);
			defaultPartsList.addPart(bett, matraze, 1);
			defaultPartsList.addPart(bett, huebscheStudentin, 2);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultPartsList;
	}

	static final String BETTGESTELL = "Bettgestell";
	static final String LATTENROST = "Lattenrost";
	static final String BETTFU�PAKET = "Bettfu�paket";
	static final String H�BSCHE_STUDENTIN = "H�bsche Studentin";
	static final String BAND = "Band";
	static final String HOLZKLEBER = "Holzkleber";
	static final String FEDER = "Feder";
	static final String MATRAZE = "Matraze";
	static final String SCHRAUBE_MUTTER = "Schraube + Mutter";
	static final String HOLZLATTE = "Holzlatte";
	static final String BETTFU� = "Bettfu�";
	static final String BETT = "Bett";

}
