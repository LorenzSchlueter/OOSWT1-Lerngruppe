package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import truthValue.AlmostTrue;
import truthValue.False;
import truthValue.Perhaps;
import truthValue.True;

class TruthValue {

	Perhaps p = Perhaps.getOrCreate();
	True t = True.getOrCreate();
	False f = False.getOrCreate();
	AlmostTrue at = AlmostTrue.getOrCreate();
	
	@Test
	void testNeg() {
		assertEquals(f, t.neg());
		assertEquals(p, at.neg());
		assertEquals(t, f.neg());
		assertEquals(at, p.neg());
	}
	@Test
	void testAndPerhaps() {
		assertEquals(p, t.andPerhaps());
		assertEquals(p, p.andPerhaps());
		assertEquals(f, f.andPerhaps());
	}
	@Test
	void testAnd() {
		assertEquals(f, f.and(f));
		assertEquals(f, f.and(p));
		assertEquals(f, f.and(t));
		assertEquals(f, p.and(f));
		assertEquals(p, p.and(p));
		assertEquals(p, p.and(t));
		assertEquals(f, t.and(f));
		assertEquals(p, t.and(p));
		assertEquals(t, t.and(t));
	}

	@Test
	void testOrPerhaps() {
		assertEquals(t, t.orPerhaps());
		assertEquals(p, p.orPerhaps());
		assertEquals(p, f.orPerhaps());
	}
	@Test
	void testOr() {
		assertEquals(f, f.or(f));
		assertEquals(p, f.or(p));
		assertEquals(t, f.or(t));
		assertEquals(p, p.or(f));
		assertEquals(p, p.or(p));
		assertEquals(t, p.or(t));
		assertEquals(t, t.or(f));
		assertEquals(t, t.or(p));
		assertEquals(t, t.or(t));
	}
}
