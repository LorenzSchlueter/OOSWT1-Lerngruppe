package truthValue;

public interface TruthValue {
	
	public TruthValue neg();
	
	public TruthValue and(TruthValue argument);
	public TruthValue andPerhaps();

	public TruthValue or(TruthValue argument);
	public TruthValue orPerhaps();

	public TruthValue andAlmostTrue();

	public TruthValue orAlmostTrue();
	
}
