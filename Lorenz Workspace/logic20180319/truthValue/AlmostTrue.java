package truthValue;

public class AlmostTrue implements TruthValue{
	
	private static AlmostTrue theAlmostTrue;
	
	public static AlmostTrue getOrCreate() {
		if (theAlmostTrue == null) 	theAlmostTrue = new AlmostTrue();
		return theAlmostTrue;
	}
	
	private AlmostTrue() {}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof AlmostTrue;
	}

	@Override
	public Perhaps neg() {
		return Perhaps.getOrCreate();
	}

	@Override
	public TruthValue and(TruthValue argument) {
		return argument.andAlmostTrue();
	}

	@Override
	public TruthValue andPerhaps() {
		return Perhaps.getOrCreate();
	}

	@Override
	public TruthValue or(TruthValue argument) {
		return argument.orAlmostTrue();
	}

	@Override
	public TruthValue orPerhaps() {
		return this;
	}

	@Override
	public TruthValue andAlmostTrue() {
		return this;
	}

	@Override
	public TruthValue orAlmostTrue() {
		return this;
	}
	
}
