package truthValue;

public class False implements TruthValue {

	private static False theFalse;
	
	public static False getOrCreate() {
		if (theFalse == null) theFalse = new False();
		return theFalse;
	}
	private False() {
	}
	
	public boolean equals(Object argument) {
		return (argument instanceof False);
	}
	@Override
	public True neg() {
		return True.getOrCreate();
	}
	@Override
	public False and(TruthValue argument) {
		return this;
	}
	@Override
	public TruthValue andPerhaps() {
		return this;
	}
	@Override
	public TruthValue or(TruthValue argument) {
		return argument;
	}
	@Override
	public TruthValue orPerhaps() {
		return Perhaps.getOrCreate();
	}
	@Override
	public TruthValue andAlmostTrue() {
		return this;
	}
	@Override
	public TruthValue orAlmostTrue() {
		return AlmostTrue.getOrCreate();
	}


}
