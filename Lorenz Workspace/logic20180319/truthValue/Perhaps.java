package truthValue;

public class Perhaps implements TruthValue {
	
	private static Perhaps thePerhaps;
	
	public static Perhaps getOrCreate() {
		if (thePerhaps == null) thePerhaps = new Perhaps();
		return thePerhaps;
	}
	
	private Perhaps() {
	}
	public boolean equals(Object argument) {
		return (argument instanceof Perhaps); 
	}

	@Override
	public AlmostTrue neg() {
		return AlmostTrue.getOrCreate();
	}

	@Override
	public TruthValue and(TruthValue argument) {
		return argument.andPerhaps();
	}

	@Override
	public TruthValue andPerhaps() {
		return this;
	}

	@Override
	public TruthValue or(TruthValue argument) {
		return argument.orPerhaps();
	}

	@Override
	public TruthValue orPerhaps() {
		return this;
	}

	@Override
	public TruthValue andAlmostTrue() {
		return this;
	}

	@Override
	public TruthValue orAlmostTrue() {
		return AlmostTrue.getOrCreate();
	}

}
