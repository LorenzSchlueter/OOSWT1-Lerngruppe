package truthValue;

public class True implements TruthValue {
	
	private static True theTrue;
	
	public static True getOrCreate() {
		if (theTrue == null) theTrue = new True();
		return theTrue;
	}
	private True() {
	}
	
	public boolean equals(Object argument) {
		return (argument instanceof True);
	}
	@Override
	public False neg() {
		return False.getOrCreate();
	}
	@Override
	public TruthValue and(TruthValue argument) {
		return argument;
	}
	@Override
	public TruthValue andPerhaps() {
		return Perhaps.getOrCreate();
	}
	@Override
	public TruthValue or(TruthValue argument) {
		return this;
	}
	@Override
	public TruthValue orPerhaps() {
		return this;
	}
	@Override
	public TruthValue andAlmostTrue() {
		return AlmostTrue.getOrCreate();
	}
	@Override
	public TruthValue orAlmostTrue() {
		return this;
	}



}
