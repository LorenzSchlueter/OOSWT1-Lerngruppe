package fraction;

import java.math.BigInteger;

public class Fraction {
	
	private static final String DIVISION_BY_ZERO = "Division by zero!";

	static public Fraction create(BigInteger enumerator, BigInteger denominator) {
		if(denominator.equals(BigInteger.ZERO)) throw new ArithmeticException(DIVISION_BY_ZERO);
			BigInteger gcd = enumerator.gcd(denominator);
			return new Fraction(enumerator.divide(gcd), denominator.divide(gcd));	
	}
	
	private BigInteger enumerator;
	private BigInteger denominator;
	
	private Fraction(BigInteger enumerator, BigInteger denominator) {
		this.enumerator = enumerator;
		this.denominator = denominator;	
	}
	
	private boolean equalsAsFraction(Fraction secondArgument) {
		return this.enumerator.multiply(secondArgument.denominator).equals(this.denominator.multiply(secondArgument.enumerator));
	}
	public boolean equals(Object argument) {
		if (argument instanceof Fraction) {
			return this.equalsAsFraction((Fraction)argument);
		} else {
			return false;
		}
	}
	
	public Fraction multiply(Fraction secondArgument) {
		BigInteger resultEnumerator = this.enumerator .multiply(secondArgument.enumerator);
		BigInteger resultDenominator = this.denominator.multiply(secondArgument.denominator); 
		return Fraction.create(resultEnumerator, resultDenominator);
	}
	
	public Fraction divide(Fraction secondArgument) {
		BigInteger resultEnumerator = this.enumerator .multiply(secondArgument.denominator);
		BigInteger resultDenominator = this.denominator.multiply(secondArgument.enumerator); 
		return Fraction.create(resultEnumerator, resultDenominator);
	}

	public Fraction add(Fraction summand) {
		BigInteger resultEnumerator = this.enumerator.multiply(summand.denominator).add(summand.enumerator.multiply(this.denominator));
		BigInteger resultDenominator = this.denominator.multiply(summand.denominator);
		return Fraction.create(resultEnumerator, resultDenominator);
	}
	
	public Fraction subtract(Fraction subtrahend) {
		BigInteger resultEnumerator = this.enumerator.multiply(subtrahend.denominator).subtract(subtrahend.enumerator.multiply(this.denominator));
		BigInteger resultDenominator = this.denominator.multiply(subtrahend.denominator);
		return Fraction.create(resultEnumerator, resultDenominator);
	}
}
