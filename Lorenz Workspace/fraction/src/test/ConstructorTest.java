package test;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

import fraction.Fraction;


class ConstructorTest {
	private Fraction half = Fraction.create(BigInteger.valueOf(1), BigInteger.valueOf(2));
	private Fraction quater = Fraction.create(BigInteger.valueOf(1), BigInteger.valueOf(2));

	
	@Test
	void testEqualsFramework() {
		assertEquals(Fraction.create(BigInteger.valueOf(1), BigInteger.valueOf(2)),Fraction.create(BigInteger.valueOf(2), BigInteger.valueOf(4)));
	}
	
	@Test
	void testExcepionalConstructor() {
		try {
			Fraction.create(BigInteger.valueOf(1), BigInteger.ZERO);
			fail();
		}catch(ArithmeticException e) {
			return;
		}
	}
	
	@Test
	void testMul1() {
		assertEquals(quater, half.multiply(half));
	}
	
}
